﻿using RegistroEstudiantes.model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using RegistroEstudiantes;

namespace RegistroEstudiantes
{

    public interface IMateriaService
    {
        IList<Materia> GetMateriasPorNombre(string texto);
        public Materia GetMateriaId(int Id );
    }
   
}


public class InMemoryMateriasService : IMateriaService
{
    IList<Materia> materias;

    public InMemoryMateriasService()
    {
        this.materias = new List<Materia>() 
        {
           new Materia{Id=1,Codigo="01",Area=Area.Informatica,Diposnible= true,Nombre="Introduccion a la programacion", Objetivos = "adquirir los conocimientos necesarios"},
           new Materia{Id=2,Codigo="02",Area=Area.Informatica,Diposnible= true,Nombre="Progamacion 1", Objetivos = "adquirir los conocimientos necesarios"},
           new Materia{Id=3,Codigo="03",Area=Area.Informatica,Diposnible= true,Nombre="Progamacion 2", Objetivos = "adquirir los conocimientos necesarios"},
           new Materia{Id=4,Codigo="04",Area=Area.Informatica, Diposnible= true,Nombre="Ingenieria de Sofware", Objetivos = "adquirir los conocimientos necesarios"},
         };
    }

    public Materia GetMateriaId(int Id)
    {
        return this.materias.SingleOrDefault(d => d.Id == Id);
    }

    public IList<Materia> GetMateriasPorNombre(string texto)
    {
        if (!string.IsNullOrEmpty(texto))
        {
            texto = texto.ToLower();
        }

       return materias.Where(m => string.IsNullOrEmpty(texto) || m.Nombre.ToLower().Contains(texto)).OrderBy(m => m.Nombre).ToList();

    }

    
}
