﻿using RegistroEstudiantes.model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using RegistroEstudiantes;
using RegistroEstudciantes;

namespace RegistroEstudciantes
{

    public interface IEsService
    {
        IList<Estudiante> GetEstudiantesPorNombre(string texto);
        public Estudiante GetEstudiantesId(int Id);
    }

}


public class InMemoryEsService : IEsService

{
    IList<Estudiante> lista;

    public InMemoryEsService()
    {
        this.lista = new List<Estudiante>()
        {
           new Estudiante{Matricula=1,Nombre="Ismael",Apellido="Paredes",Edad=23,Sexo="M"},
           new Estudiante{Matricula=2,Nombre="Jorge",Apellido="Paniagua",Edad=24,Sexo="M"},
           new Estudiante{Matricula=3,Nombre="Roberto",Apellido="Guerrero",Edad=30,Sexo="M"},
           new Estudiante{Matricula=4,Nombre="Cesar",Apellido="Lara",Edad=30,Sexo="M"},

         };
    }

    public Estudiante GetEstudiantesId(int maticula)
    {
        return this.lista.SingleOrDefault(d => d.Matricula == maticula);
    }


    public IList<Estudiante> GetAGetEstudiantesPorNombrell(string texto)
    {
        if(!string.IsNullOrEmpty(texto))
        {
            texto = texto.ToLower();
        }

        return lista.Where(m => string.IsNullOrEmpty(texto) || m.Nombre.ToLower().Contains(texto)).OrderBy(m => m.Nombre).ToList();
    }


    public IList<Estudiante> GetEstudiantesPorNombre(string texto)
    {
        if (!string.IsNullOrEmpty(texto))
        {
            texto = texto.ToLower();
        }

        return lista.Where(m => string.IsNullOrEmpty(texto) || m.Nombre.ToLower().Contains(texto)).OrderBy(m => m.Nombre).ToList();
    }
}