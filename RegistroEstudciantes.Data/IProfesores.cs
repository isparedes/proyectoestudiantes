﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RegistroEstudciantes;
using RegistroEstudiantes.model;

namespace RegistroEstudciantes
{
   public interface IProfesores
    {
        IList<Profesores> GetAll();
    }
}

public class InMemoryProfesoresService : IProfesores
{
    IList<Profesores> profesores;

    public InMemoryProfesoresService(){
        this.profesores = new List<Profesores>()
        {
            new Profesores{Id=1,Nombre="Juan",Area="Sofware",sexo="M",Disponible=true},
            new Profesores{Id=1,Nombre="Juan",Area="Sofware",sexo="M",Disponible=true},
            new Profesores{Id=1,Nombre="Juan",Area="Sofware",sexo="M",Disponible=true},
            new Profesores{Id=1,Nombre="Juan",Area="Sofware",sexo="M",Disponible=true}
        };
    }

    public IList<Profesores> GetAll()
    {
        return this.profesores.OrderBy(p => p.Nombre).ToList();
    }

}
