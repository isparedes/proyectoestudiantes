using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RegistroEstudciantes;
using RegistroEstudiantes.model;

namespace RegistroEstudiantes.Pages
{
    public class ListaProfesoresModel : PageModel
    {

        
        public IProfesores Iprofesores;
        public IList<Profesores> lista { get; set; }

        public ListaProfesoresModel(IProfesores profesoresservices)
        {

            Iprofesores = profesoresservices;
        }

        public void OnGet()
        {
          
            this.lista = Iprofesores.GetAll();
        }
    }
}
