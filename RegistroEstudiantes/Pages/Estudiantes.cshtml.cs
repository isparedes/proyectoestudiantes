using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using RegistroEstudciantes;
using RegistroEstudiantes.model;

namespace RegistroEstudiantes.Pages
{
    public class RegistroEsModel : PageModel
    {
        private IConfiguration config;
        public IEsService Estudian;
        public IList<Estudiante> Estudiante{ get; set; }

        public RegistroEsModel(IConfiguration config, IEsService estudianteService)
        {
            this.config = config;
            Estudian = estudianteService;
        }

        public string Mensaje { get; set; }



        public void OnGet(string texto)
        {
            this.Mensaje = config["Mensaje"];
            this.Estudiante= Estudian.GetEstudiantesPorNombre(texto);
        }
    }
}


