using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using RegistroEstudiantes.model;

namespace RegistroEstudiantes.Pages
{
    public class RegistroMateriasModel : PageModel
    {
     

        private IConfiguration config;
        public IMateriaService MateriaService;
        public IList<Materia> Materias { get; set; }

        public RegistroMateriasModel(IConfiguration config, IMateriaService materiaService)
        {
            this.config = config;
            MateriaService = materiaService;
        }

        public string Mensaje { get; set; }
            
        public void OnGet(string texto)
        {
            this.Mensaje = config["Mensaje"];
            this.Materias = MateriaService.GetMateriasPorNombre(texto);
        } 
    }
}
