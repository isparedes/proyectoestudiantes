﻿namespace RegistroEstudiantes.model
{
    public enum Area
    {
        N_A,
        Ingenieria,
        Informatica,
        General,
        Ciencias_Sociales
    }
}
