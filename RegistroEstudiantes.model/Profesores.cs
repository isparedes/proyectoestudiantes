﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RegistroEstudiantes.model
{
   public  class Profesores
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Area { get; set; }
        public string sexo { get; set; }
        public bool Disponible { get; set; }
    }
}
