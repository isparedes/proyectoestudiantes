﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RegistroEstudiantes.model
{
    public class Estudiante
    {
        public int Matricula { get; set; }
        public string  Nombre { get; set; }
        public string  Apellido { get; set; }
        public int  Edad { get; set; }
        public string Sexo { get; set; }
    }
}
